/*
 * gulkan
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <glib.h>
#include "gulkan-descriptor-set.h"

gboolean
gulkan_init_descriptor_pool (GulkanDevice               *device,
                             const VkDescriptorPoolSize *pool_sizes,
                             uint32_t                    pool_size_count,
                             uint32_t                    set_count,
                             VkDescriptorPool           *pool)
{
  VkDescriptorPoolCreateInfo info = {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
    .maxSets = set_count,
    .poolSizeCount = pool_size_count,
    .pPoolSizes = pool_sizes
  };

  VkDevice vk_device = gulkan_device_get_handle (device);

  VkResult res = vkCreateDescriptorPool (vk_device, &info, NULL, pool);
  vk_check_error ("vkCreateDescriptorPool", res, FALSE)

  return TRUE;
}

gboolean
gulkan_allocate_descritpor_set (GulkanDevice          *device,
                                VkDescriptorPool       pool,
                                VkDescriptorSetLayout *layout,
                                uint32_t               count,
                                VkDescriptorSet       *set)
{
  VkDescriptorSetAllocateInfo alloc_info = {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
    .descriptorPool = pool,
    .descriptorSetCount = count,
    .pSetLayouts = layout
  };

  VkDevice vk_device = gulkan_device_get_handle (device);

  VkResult res = vkAllocateDescriptorSets (vk_device, &alloc_info, set);
  vk_check_error ("vkAllocateDescriptorSets", res, FALSE)

  return TRUE;
}
