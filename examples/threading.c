/*
 * gulkan
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "gulkan.h"

typedef struct Example
{
  GulkanTexture *texture;
  GLFWwindow *window;
  GMainLoop *loop;
  VkSurfaceKHR surface;
  GulkanRenderer *renderer;
  bool should_quit;
  GdkPixbuf *src_pixbuf;
  GdkPixbuf *pixbuf;
  float factor;
  float step;
  gboolean running;

  GMutex upload_mutex;
  GCond upload_cond;
  bool uploading;

} Example;

static GdkPixbuf *
load_gdk_pixbuf ()
{
  GError *error = NULL;
  GdkPixbuf * pixbuf_no_alpha =
    gdk_pixbuf_new_from_resource ("/res/cat.jpg", &error);

  if (error != NULL) {
    g_printerr ("Unable to read file: %s\n", error->message);
    g_error_free (error);
    return NULL;
  } else {
    GdkPixbuf *pixbuf = gdk_pixbuf_add_alpha (pixbuf_no_alpha, false, 0, 0, 0);
    g_object_unref (pixbuf_no_alpha);
    return pixbuf;
  }
}

static void
key_callback (GLFWwindow* window, int key,
              int scancode, int action, int mods)
{
  (void) scancode;
  (void) mods;

  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
      Example *self = (Example*) glfwGetWindowUserPointer (window);
      self->should_quit = true;
    }
}

static void
init_glfw (Example *self, int width, int height)
{
  glfwInit();

  glfwWindowHint (GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint (GLFW_RESIZABLE, false);

  self->window = glfwCreateWindow (width, height, "Vulkan Pixbuf", NULL, NULL);

  glfwSetKeyCallback (self->window, key_callback);

  glfwSetWindowUserPointer (self->window, self);
}

static gboolean
input_cb (gpointer data)
{
  Example *self = (Example*) data;

  glfwPollEvents ();
  if (glfwWindowShouldClose (self->window) || self->should_quit)
    {
      self->running = false;
      return FALSE;
    }

  return TRUE;
}

static uint32_t uploadnum = 0;

static void*
reupload_cb (gpointer data)
{
  Example *self = (Example*) data;

  while (self->running)
    {
      usleep(14000);

      uploadnum++;

      if (self->factor > 10.0f || self->factor <= 0.0f)
        self->step = -self->step;

      self->factor += self->step;

      g_print ("Locking upload %d\n", uploadnum);

      g_mutex_lock (&self->upload_mutex);

      self->uploading = true;

      gdk_pixbuf_saturate_and_pixelate (self->src_pixbuf, self->pixbuf,
                                        self->factor, FALSE);

      g_object_unref (self->texture);

      GulkanClient *client = GULKAN_CLIENT (self->renderer);

      self->texture =
        gulkan_client_texture_new_from_pixbuf (client, self->pixbuf,
                                               VK_FORMAT_R8G8B8A8_UNORM,
                                               VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                               true);

      gulkan_renderer_update_texture (self->renderer, self->texture);

      self->uploading = false;

      g_cond_signal (&self->upload_cond);
      g_mutex_unlock (&self->upload_mutex);

      g_print ("Unlocked upload %d\n", uploadnum);
    }

  return NULL;
}

static void
_render (Example *self, guint i)
{

  g_mutex_lock (&self->upload_mutex);

  while (self->uploading)
    g_cond_wait (&self->upload_cond, &self->upload_mutex);

  g_print ("We render. %d\n", i);

  gulkan_renderer_draw (self->renderer);

  GulkanClient *client = GULKAN_CLIENT (self->renderer);
  gulkan_device_wait_idle (gulkan_client_get_device (client));

  g_mutex_unlock (&self->upload_mutex);
}

static void*
_render_thread_cb (gpointer _self)
{
  Example *self = (Example*) _self;

  g_print("Starting render thread. Running %d\n", self->running);

  guint i = 0;
  while (self->running && i < 1000)
    {
      _render (self, i);
      i++;
      usleep(7000);
    }

  self->running = false;

  g_print("Not running anymore, bye.\n");

  if (self->loop != NULL)
    g_main_loop_quit (self->loop);

  return NULL;
}

int
main () {
  Example example = {
    .should_quit = false,
    .factor = 1.0f,
    .step = 0.5f,
    .running = TRUE,
    .uploading = false
  };

  g_mutex_init (&example.upload_mutex);
  g_cond_init (&example.upload_cond);

  example.src_pixbuf = load_gdk_pixbuf ();
  if (example.src_pixbuf == NULL)
    return -1;

  example.pixbuf = load_gdk_pixbuf ();
  if (example.pixbuf == NULL)
    return -1;

  gint width = gdk_pixbuf_get_width (example.pixbuf);
  gint height = gdk_pixbuf_get_height (example.pixbuf);

  init_glfw (&example, width / 2, height / 2);

  example.loop = g_main_loop_new (NULL, FALSE);

  example.renderer = gulkan_renderer_new ();

  uint32_t num_glfw_extensions = 0;
  const char** glfw_extensions;
  glfw_extensions = glfwGetRequiredInstanceExtensions (&num_glfw_extensions);

  GSList *instance_ext_list = NULL;
  for (uint32_t i = 0; i < num_glfw_extensions; i++)
    {
      g_print ("Instance extensions: %s\n", glfw_extensions[i]);
      char *instance_ext = g_strdup (glfw_extensions[i]);
      instance_ext_list = g_slist_append (instance_ext_list, instance_ext);
    }

  const gchar *instance_extensions[] =
  {
    VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME
  };
  for (uint32_t i = 0; i < G_N_ELEMENTS (instance_extensions); i++)
    {
      char *instance_ext = g_strdup (instance_extensions[i]);
      instance_ext_list = g_slist_append (instance_ext_list, instance_ext);
    }

  const gchar *device_extensions[] =
  {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
  };

  GSList *device_ext_list = NULL;
  for (uint64_t i = 0; i < G_N_ELEMENTS (device_extensions); i++)
    {
      char *device_ext = g_strdup (device_extensions[i]);
      device_ext_list = g_slist_append (device_ext_list, device_ext);
    }

  GulkanClient *client = GULKAN_CLIENT (example.renderer);
  if (!gulkan_client_init_vulkan (client,
                                  instance_ext_list,
                                  device_ext_list))
  {
    g_printerr ("Unable to initialize Vulkan!\n");
    return -1;
  }

  VkInstance instance = gulkan_client_get_instance_handle (client);
  if (glfwCreateWindowSurface (instance,
                               example.window, NULL,
                              &example.surface) != VK_SUCCESS)
    {
      g_printerr ("Unable to create surface.\n");
      return -1;
    }

  example.texture =
    gulkan_client_texture_new_from_pixbuf (client, example.pixbuf,
                                           VK_FORMAT_R8G8B8A8_UNORM,
                                           VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                           true);

  if (!gulkan_renderer_init_rendering (example.renderer,
                                       example.surface,
                                       example.texture))
    return -1;

  g_timeout_add (7, input_cb, &example);

  GError *error = NULL;
  GThread *render_thread = g_thread_try_new ("render",
                                             (GThreadFunc) _render_thread_cb,
                                             &example,
                                             &error);
  if (error != NULL)
    {
      g_printerr ("Unable to start render thread: %s\n", error->message);
      g_error_free (error);
    }

  GThread *upload_thread = g_thread_try_new ("upload",
                                             (GThreadFunc) reupload_cb,
                                             &example,
                                             &error);
  if (error != NULL)
    {
      g_printerr ("Unable to start render thread: %s\n", error->message);
      g_error_free (error);
    }

  g_main_loop_run (example.loop);

  g_thread_join (upload_thread);
  g_thread_join (render_thread);

  g_print("We joined the render thread!\n");

  g_mutex_clear (&example.upload_mutex);
  g_cond_clear (&example.upload_cond);

  g_main_loop_unref (example.loop);

  g_slist_free (instance_ext_list);
  g_slist_free (device_ext_list);

  g_object_unref (example.pixbuf);
  g_object_unref (example.src_pixbuf);
  g_object_unref (example.texture);
  g_object_unref (example.renderer);

  glfwDestroyWindow (example.window);
  glfwTerminate ();

  return 0;
}
