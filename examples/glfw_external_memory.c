/*
 * gulkan
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 *
 * based on code from
 * https://github.com/lostgoat/ogl-samples/blob/master/tests/gl-450-culling.cpp
 * https://gitlab.com/beVR_nz/VulkanIPC_Demo/
 */

#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <GL/glew.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "gulkan.h"

#define ENUM_TO_STR(r) case r: return #r

static const gchar*
gl_error_string (GLenum code)
{
  switch (code)
    {
      ENUM_TO_STR(GL_NO_ERROR);
      ENUM_TO_STR(GL_INVALID_ENUM);
      ENUM_TO_STR(GL_INVALID_VALUE);
      ENUM_TO_STR(GL_INVALID_OPERATION);
      ENUM_TO_STR(GL_INVALID_FRAMEBUFFER_OPERATION);
      ENUM_TO_STR(GL_OUT_OF_MEMORY);
      ENUM_TO_STR(GL_STACK_UNDERFLOW);
      ENUM_TO_STR(GL_STACK_OVERFLOW);
      default:
        return "UNKNOWN GL Error";
    }
}

typedef struct Example
{
  GulkanTexture *texture;
  GLFWwindow *window;
  GLFWwindow *opengl_window;
  GMainLoop *loop;
  VkSurfaceKHR surface;
  GulkanRenderer *renderer;
  bool should_quit;
} Example;

static GdkPixbuf *
load_gdk_pixbuf ()
{
  GError *error = NULL;
  GdkPixbuf * pixbuf_no_alpha =
    gdk_pixbuf_new_from_resource ("/res/cat.jpg", &error);

  if (error != NULL) {
    g_printerr ("Unable to read file: %s\n", error->message);
    g_error_free (error);
    return NULL;
  } else {
    GdkPixbuf *pixbuf = gdk_pixbuf_add_alpha (pixbuf_no_alpha, false, 0, 0, 0);
    g_object_unref (pixbuf_no_alpha);
    return pixbuf;
  }
}

static void
key_callback (GLFWwindow* window, int key,
              int scancode, int action, int mods)
{
  (void) scancode;
  (void) mods;

  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
      Example *self = (Example*) glfwGetWindowUserPointer (window);
      self->should_quit = true;
    }
}

static void
init_glfw (Example *self, int width, int height)
{
  glfwInit();

  glfwWindowHint (GLFW_CLIENT_API, GLFW_NO_API);
  self->window = glfwCreateWindow (width, height, "GLFW VK", NULL, NULL);
  glfwSetKeyCallback (self->window, key_callback);

  glfwSetWindowUserPointer (self->window, self);


  glfwWindowHint (GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint (GLFW_RESIZABLE, false);

  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  self->opengl_window = glfwCreateWindow (width, height, "GLFW OpenGL", NULL, NULL);
  glfwMakeContextCurrent (self->opengl_window);
  glfwHideWindow (self->opengl_window);
}

static gboolean
draw_cb (gpointer data)
{
  Example *self = (Example*) data;

  glfwPollEvents ();
  if (glfwWindowShouldClose (self->window) || self->should_quit)
    {
      g_main_loop_quit (self->loop);
      return FALSE;
    }

  gulkan_renderer_draw (self->renderer);

  GulkanClient *client = GULKAN_CLIENT (self->renderer);
  gulkan_device_wait_idle (gulkan_client_get_device (client));

  return TRUE;
}

static void
gl_check_error (char* prefix)
{
    GLenum err = 0 ;
    while((err = glGetError ()) != GL_NO_ERROR)
      g_printerr ("GL ERROR: %s - %s\n", prefix, gl_error_string (err));
}

int
main () {
  Example example = {
    .should_quit = false
  };

  GdkPixbuf * pixbuf = load_gdk_pixbuf ();
  if (pixbuf == NULL)
    return -1;

  gint width = gdk_pixbuf_get_width (pixbuf);
  gint height = gdk_pixbuf_get_height (pixbuf);

  init_glfw (&example, width / 2, height / 2);

  example.loop = g_main_loop_new (NULL, FALSE);

  example.renderer = gulkan_renderer_new ();

  uint32_t num_glfw_extensions = 0;
  const char** glfw_extensions;
  glfw_extensions = glfwGetRequiredInstanceExtensions (&num_glfw_extensions);

  GSList *instance_ext_list = NULL;
  for (uint32_t i = 0; i < num_glfw_extensions; i++)
    {
      char *instance_ext = g_strdup (glfw_extensions[i]);
      instance_ext_list = g_slist_append (instance_ext_list, instance_ext);
    }
  instance_ext_list = g_slist_append (instance_ext_list,
                        VK_KHR_EXTERNAL_FENCE_CAPABILITIES_EXTENSION_NAME);
  instance_ext_list = g_slist_append (instance_ext_list,
                        VK_KHR_EXTERNAL_MEMORY_CAPABILITIES_EXTENSION_NAME);
  instance_ext_list = g_slist_append (instance_ext_list,
                        VK_KHR_EXTERNAL_SEMAPHORE_CAPABILITIES_EXTENSION_NAME);
  instance_ext_list = g_slist_append (instance_ext_list,
                        VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME);


  const gchar *device_extensions[] =
  {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    VK_KHR_EXTERNAL_FENCE_EXTENSION_NAME,
    VK_KHR_EXTERNAL_FENCE_FD_EXTENSION_NAME,
    VK_KHR_EXTERNAL_MEMORY_EXTENSION_NAME,
    VK_KHR_EXTERNAL_MEMORY_FD_EXTENSION_NAME,
    VK_KHR_EXTERNAL_SEMAPHORE_EXTENSION_NAME,
    VK_KHR_EXTERNAL_SEMAPHORE_FD_EXTENSION_NAME,
    VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME,
    VK_KHR_DEDICATED_ALLOCATION_EXTENSION_NAME
  };

  GSList *device_ext_list = NULL;
  for (uint64_t i = 0; i < G_N_ELEMENTS (device_extensions); i++)
    {
      char *device_ext = g_strdup (device_extensions[i]);
      device_ext_list = g_slist_append (device_ext_list, device_ext);
    }

  GulkanClient *client = GULKAN_CLIENT (example.renderer);
  if (!gulkan_client_init_vulkan (client,
                                  instance_ext_list,
                                  device_ext_list))
  {
    g_printerr ("Unable to initialize Vulkan!\n");
    return -1;
  }

  VkInstance instance = gulkan_client_get_instance_handle (client);
  GulkanDevice *device = gulkan_client_get_device (client);
  if (glfwCreateWindowSurface (instance,
                               example.window, NULL,
                              &example.surface) != VK_SUCCESS)
    {
      g_printerr ("Unable to create surface.\n");
      return -1;
    }

  GLenum err = glewInit ();
  if (GLEW_OK != err)
    {
      g_printerr ("glewInit Error: %s\n", glewGetErrorString (err));
      return 1;
    }

  if (!glewIsSupported ("GL_EXT_memory_object"))
  {
    g_printerr ("GL_EXT_memory_object is not supported.\n");
    return -1;
  }

  int fd;
  gsize size;
  example.texture = gulkan_texture_new_export_fd (
    device, (guint)width, (guint)height, VK_FORMAT_R8G8B8A8_UNORM, &size, &fd);

  if (!example.texture)
    {
      g_printerr ("Could not initialize texture.\n");
      return -1;
    }

  GLint gl_dedicated_mem = GL_TRUE;

  GLuint gl_mem_object = 0;
  glCreateMemoryObjectsEXT (1, &gl_mem_object);
  gl_check_error ("glCreateMemoryObjectsEXT");

  g_print ("created memory object id: %d\n", gl_mem_object);

  glMemoryObjectParameterivEXT (gl_mem_object, GL_DEDICATED_MEMORY_OBJECT_EXT,
                                &gl_dedicated_mem);
  gl_check_error ("glCreateMemoryObjectsEXT");

  /* Note: ImportMemoryFd takes ownership of the fd from the Vulkan driver.
   * The Vulkan driver is now free to reuse the fd number */
  glImportMemoryFdEXT (gl_mem_object, size, GL_HANDLE_TYPE_OPAQUE_FD_EXT, fd);
  gl_check_error ("glImportMemoryFdEXT");

  g_print ("Imported texture from fd %d\n", fd);

  GLuint gl_texture;
  glGenTextures (1, &gl_texture);
  glBindTexture (GL_TEXTURE_2D, gl_texture);
  gl_check_error ("glBindTexture");

  glTexParameteri (GL_TEXTURE_2D,GL_TEXTURE_TILING_EXT, GL_OPTIMAL_TILING_EXT);
  gl_check_error ("glTexParameteri GL_OPTIMAL_TILING_EXT");
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  gl_check_error ("glTexParameteri GL_TEXTURE_MIN_FILTER");
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl_check_error ("glTexParameteri GL_TEXTURE_MAG_FILTER");

  glTexStorageMem2DEXT (GL_TEXTURE_2D, 1, GL_RGBA8, width, height,
                        gl_mem_object, 0);
  gl_check_error ("glTexStorageMem2DEXT");

  guchar *rgb = gdk_pixbuf_get_pixels (pixbuf);

  /* we can NOT use glTexImage2D() because the memory is already allocated */
  glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA,
                   GL_UNSIGNED_BYTE, (GLvoid*)rgb);
  gl_check_error ("glTexSubImage2D");

  glFinish();

  if (!gulkan_client_transfer_layout (client,
                                      example.texture,
                                      VK_IMAGE_LAYOUT_UNDEFINED,
                                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL))
    {
      g_printerr ("Could not transfer image layout!");
      /* not fatal, will just fail validation now */
    }

  if (!gulkan_renderer_init_rendering (example.renderer,
                                       example.surface,
                                       example.texture))
    return -1;

  g_timeout_add (20, draw_cb, &example);
  g_main_loop_run (example.loop);
  g_main_loop_unref (example.loop);

  glDeleteTextures (1, &gl_texture);
  glDeleteMemoryObjectsEXT (1, &gl_mem_object);

  g_slist_free (instance_ext_list);
  g_slist_free (device_ext_list);

  g_object_unref (pixbuf);
  g_object_unref (example.texture);
  g_object_unref (example.renderer);

  glfwDestroyWindow (example.window);
  glfwTerminate ();

  return 0;
}

